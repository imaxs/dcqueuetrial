namespace Canvas.Core
{
    public interface IAnimatorable
    {
        IAnimator Animator { get; }
    }
}