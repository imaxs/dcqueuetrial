using System;
using UnityEngine.EventSystems;

namespace Canvas.Core
{
    public interface IInteractiveCanvas : ICanvas
    {
        event Action OnTapped;
        void OnPointerDown(PointerEventData pointerEventData);
    }
}