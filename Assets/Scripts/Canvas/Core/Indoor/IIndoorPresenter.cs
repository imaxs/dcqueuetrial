using System;
using UnityEngine;
using Canvas.Core;
namespace Canvas.Core.Presenters
{
    public interface IIndoorPresenter : IPresenter
    {
        void Placement(Vector2 target);
    }
}