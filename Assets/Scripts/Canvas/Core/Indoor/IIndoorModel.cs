using UnityEngine;
namespace Canvas.Core.Models
{
    public interface IIndoorModel : ISettingable
    {
        IField<int> StoreysNumber { get; }
        AudioClip[] AudioClips { get; }
    }
}