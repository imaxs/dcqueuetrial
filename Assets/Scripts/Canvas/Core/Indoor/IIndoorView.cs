namespace Canvas.Core.Views
{
    public interface IIndoorView<out T> : IView<T>, IInteractiveCanvas, ISoundable
    {
         
    }
}