using System;
namespace Canvas.Core
{
    public interface IMovementController
    {
        event Action OnStartMoving;
        event Action OnMoveCompleted;
        void MoveToTarget(int floor, float x, float speed, float height = 0, float yoffset = 0);
    }
}