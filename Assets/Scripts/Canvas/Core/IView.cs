namespace Canvas.Core
{
    public interface IView<out T>
    {
        T Presenter { get; }
    }
}