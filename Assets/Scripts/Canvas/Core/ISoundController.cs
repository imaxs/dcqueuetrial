using System;
using UnityEngine;

namespace Canvas.Core
{
    public interface ISoundController
    {
        event Action OnPlay;
        event Action OnStop;
        void PlaySound(int clipIndex = 0);
        void StopSound();
        void Initialize(AudioClip[] clips);
    }
}