namespace Canvas.Core
{
    public interface IService<out T>
    {
        bool IsInit { get; }
        T Instance { get; }
    }
}