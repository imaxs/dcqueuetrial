using UnityEngine;
namespace Canvas.Core
{
    public interface IPoints : ICanvas
    {
        Vector2[] GetPointsBetween(float y1, float y2);
    }
}