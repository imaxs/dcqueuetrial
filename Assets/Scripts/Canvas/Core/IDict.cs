using System;
using System.Collections;
using System.Collections.Generic;

namespace Canvas.Core
{
    public interface IDict
    {
        void Add<T>(IList items);
        void Add<T>(T item);
        void Add(Type type, object item);
        void Add(object item);
        bool ContainsKey<T>();
        bool Contains<T>(T item);
        void Remove<T>(T item);
        void Remove(Type type, object item);
        void Remove(object item);
        IList TryGet<T>();
        IList<T> Values<T>(bool isImplementing = false);
        int Length<T>(bool isImplementing = false);
        int Count();
        IEnumerator<KeyValuePair<Type, IList>> GetEnumerator();
    }
}