using System;
namespace Canvas.Core
{
    public interface IAnimator
    {
        void Idle();
        void Moving();
        void Knock();
        event Action OnAnimationCompleted;
    }
}