using UnityEngine;
using Canvas.ScriptableObjects;
namespace Canvas.Core
{
    public interface ISettings
    {
        AudioClip[] MusicFiles { get; }
        float MusicVolume { get; }
        float SoundVolume { get; }
        CharacterPreset[] CharacterPrefabs { get; }
        IndoorPreset[] IndoorPrefab { get; }
    }
}