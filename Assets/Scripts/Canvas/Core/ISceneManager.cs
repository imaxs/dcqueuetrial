using System;
using System.Collections.Generic;
namespace Canvas.Core
{
    public interface ISceneManager
    {
        void RegisterItem<T>(T item);
        void RegisterItem(Type type, object item);
        void UnregisterItem<T>(T item);
        void UnregisterItem(Type type, object item);
        IList<T> Items<T>(bool isImplementing = false);
        void PrintDebug();
    }
}