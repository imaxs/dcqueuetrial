using System;
namespace Canvas.Core
{
    public interface IPresentEvent<T>
    {
        int Length { get; }
        void Subscribe(Action<T> action);
        void Unsubscribe(Action<T> action);
        void Publish(T message);
    }
}