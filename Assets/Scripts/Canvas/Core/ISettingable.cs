namespace Canvas.Core
{
    public interface ISettingable
    {
        ISettings Dataset { get; }
    }
}