using System;
namespace Canvas.Core
{
    public interface IField<T>
    {
        T Value { get; set; }
        event Action<T> OnChanged;
    }
}