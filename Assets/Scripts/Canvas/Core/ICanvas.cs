using System;
using UnityEngine;
using UnityEngine.UI;

namespace Canvas.Core
{
    public interface ICanvas
    {
        Guid Uid { get; }
        CanvasScaler Scaler { get; }
        float ScalarRatio { get; }
        Vector2 ScalarRatioXY { get; }
        RectTransform Rect { get; }
        UnityEngine.Canvas Canv { get; }
        event Action OnShowed;
        event Action OnHidden;
        event Action OnStart;
    }
}