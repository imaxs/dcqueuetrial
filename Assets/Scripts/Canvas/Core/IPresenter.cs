namespace Canvas.Core
{
    public interface IPresenter
    {
        void Enable();
        void Disable();
    }
}