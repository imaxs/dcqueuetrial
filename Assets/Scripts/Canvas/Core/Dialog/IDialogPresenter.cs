using System;
namespace Canvas.Core.Presenters
{
    public interface IDialogPresenter : IPresenter
    {
        event Action OnShowed;
        event Action OnHidden;
    }
}