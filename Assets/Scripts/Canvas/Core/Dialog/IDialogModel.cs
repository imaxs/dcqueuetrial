namespace Canvas.Core.Models
{
    public interface IDialogModel
    {
        IField<int> TimerSeconds { get; }
    }
}