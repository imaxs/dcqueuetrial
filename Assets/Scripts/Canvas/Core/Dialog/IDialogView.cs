using UnityEngine.UI;
namespace Canvas.Core.Views
{
    public enum DialogType
    {
        Normal,
        Attention
    }

    public interface IDialogView<out T> : IView<T>, IInteractiveCanvas
    {
        DialogType Type { get; }
        Image Background { get; }
        Image Icon { get; }
        Image Timer { get; }
        IController Controller { get; }
    }
}