using System;
namespace Canvas.Core
{
    public interface IController
    {
        event Action OnShowed;
        event Action OnHidden;
        void Show();
        void Hide();
        void Success();
        void Failure();
        void ShowTimer(float duration);
    }
}