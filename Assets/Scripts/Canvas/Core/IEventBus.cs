using System;
namespace Canvas.Core
{
    public interface IEventBus
    {
        void Subscribe<T1>(Action<T1> action) where T1 : IEventArgs;
        void Unsubscribe<T1>(Action<T1> action) where T1 : IEventArgs;
        void Publish<T1>(T1 args) where T1 : IEventArgs;
    }
}