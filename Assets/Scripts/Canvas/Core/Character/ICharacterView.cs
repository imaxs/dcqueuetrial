namespace Canvas.Core.Views
{
    public interface ICharacterView<out T> : IView<T>, IInteractiveCanvas, IMovable, IAnimatorable, ISoundable
    {

    }
}