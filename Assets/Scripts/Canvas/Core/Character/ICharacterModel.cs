using UnityEngine;
namespace Canvas.Core.Models
{
    public interface ICharacterModel : ISettingable
    {
        AudioClip[] AudioClips { get; }
        IField<float> SpeedMovement { get; }
        IField<int> Level { get; }
    }
}