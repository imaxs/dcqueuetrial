namespace Canvas.Core
{
    public interface IControllable
    {
        IController Controller { get; }
    }
}