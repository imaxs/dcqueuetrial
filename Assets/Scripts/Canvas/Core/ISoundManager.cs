using UnityEngine;
namespace Canvas.Core
{
    public enum SoundChannel
    {
        First = 0,
        Second = 1,
        Third = 2,
        // ---------
        Max = Third,
        Auto = -1,
        None = -1
    }

    public interface ISoundManager
    {
        void PlaySound(string file, bool isLooped = false, SoundChannel channel = SoundChannel.Auto);
        void PlaySound(AudioClip audio, bool isLooped = false, SoundChannel channel = SoundChannel.Auto);
    }
}