namespace Canvas.Core
{
    public interface IMovable
    {
        IMovementController Controller { get; }
    }
}