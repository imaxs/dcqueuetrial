namespace Canvas.Core
{
    public interface ISoundable
    {
        ISoundController SoundController { get;}
    }
}