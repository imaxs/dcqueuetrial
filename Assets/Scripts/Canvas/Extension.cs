using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

namespace Canvas
{
    public static class Extension
    {
        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> enumerable)
        {
            foreach (var cur in enumerable)
                collection.Add(cur);
        }

        public static T[] Find<T>()
        {
            List<T> interfaces = new List<T>();
            GameObject[] rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
            foreach (var rootGameObject in rootGameObjects)
            {
                T[] childrenInterfaces = rootGameObject.GetComponentsInChildren<T>();
                foreach (var childInterface in childrenInterfaces)
                {
                    interfaces.Add(childInterface);
                }
            }
            return interfaces.ToArray();
        }

        public static T[] GetInChildrenWithoutRoot<T>(this GameObject obj, bool includeInactive = false)
        {
            var root = obj.GetInstanceID();
            var cmps = obj.GetComponentsInChildren<T>(includeInactive);
            Debug.Log(typeof(T) + " " + cmps.Length);
            if (cmps == null || cmps.Length == 0) return null;
            int i = (cmps[0] as MonoBehaviour).GetInstanceID() == root ? 1 : 0;
            int length = cmps.Length - 1 - i;
            if (length == 0) return null;
            T[] result = new T[length];
            for (i = 0; i < result.Length; i++) result[i] = cmps[i + 1];
            return result;
        }

        public static T GetValue<T>(this T[] array, int index) where T : class
        {
            return array == null ? null : array[index];
        }

        public static GameObject[] FindGameObjectsWithName(string name)
        {
            var result = new List<GameObject>();
            GameObject[] rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
            foreach (GameObject obj in rootGameObjects)
                if (obj.name == name)
                    result.Add(obj);
            return result.Count == 0 ? null : result.ToArray();
        }

        public static Vector2 Unscale(this Vector2 vector, CanvasScaler scaler)
        {
            var ratio = scaler.GetScaleRatio();
            return vector * Mathf.Lerp(ratio.x, ratio.y, scaler.matchWidthOrHeight);
        }

        public static float GetRation(this CanvasScaler scaler)
        {
            var ratio = scaler.GetScaleRatio();
            return Mathf.Lerp(ratio.x, ratio.y, scaler.matchWidthOrHeight);
        }

        public static Vector2 GetScaleRatio(this CanvasScaler scaler)
        {
            float widthRatio = Screen.width / scaler.referenceResolution.x;
            float heightRatio = Screen.height / scaler.referenceResolution.y;
            return new Vector2(widthRatio, heightRatio);
        }

        public static Vector3 WorldToCanvasPosition(this RectTransform rect, Vector3 worldPosition, Camera camera = null)
        {
            if (camera == null) camera = Camera.main;
            var viewportPosition = camera.WorldToViewportPoint(worldPosition);
            return rect.ViewportToCanvasPosition(viewportPosition);
        }

        public static Vector3 ScreenToCanvasPosition(this RectTransform rect, Vector3 screenPosition)
        {
            var viewportPosition = new Vector3(screenPosition.x / Screen.width,
                                               screenPosition.y / Screen.height,
                                               0);
            return rect.ViewportToCanvasPosition(viewportPosition);
        }

        public static Vector3 ViewportToCanvasPosition(this RectTransform rect, Vector3 viewportPosition)
        {
            var centerBasedViewPortPosition = viewportPosition - new Vector3(0.5f, 0.5f, 0);
            var scale = rect.sizeDelta;
            return Vector3.Scale(centerBasedViewPortPosition, scale);
        }

        public static Vector2 WorldToLocalPointInRectangle(this Vector2 worldpoint, RectTransform rect, Camera camera = null)
        {
            if (camera == null) camera = Camera.main;
            Vector3 screenPoint = camera.WorldToScreenPoint(worldpoint);
            Vector2 result;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(rect, screenPoint, camera, out result);
            return result;
        }

        public static Vector2 Scaling(this Vector2 word, CanvasScaler scaler)
        {
            return new Vector2(word.x, word.y - (scaler.referenceResolution.y - Screen.height) * scaler.scaleFactor);
        }

        public static Vector2 Scaling(this Vector3 word, CanvasScaler scaler)
        {
            return new Vector2(word.x, word.y - (scaler.referenceResolution.y - Screen.height) * scaler.scaleFactor);
        }
    }
}