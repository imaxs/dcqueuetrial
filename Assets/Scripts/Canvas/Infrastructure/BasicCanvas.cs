using System;
using UnityEngine;
using UnityEngine.UI;
using Canvas.Core;

namespace Canvas.Infrastructure
{
    public abstract class BasicCanvas : MonoBehaviour, ICanvas
    {
        private Guid m_Uid;
        public Guid Uid { get => m_Uid; }
        private CanvasScaler m_CanvasScaler;
        public CanvasScaler Scaler { get => m_CanvasScaler; }
        private float m_ScalarRatio;
        public float ScalarRatio { get => m_ScalarRatio; }
        private Vector2 m_ScalarRatioXY;
        public Vector2 ScalarRatioXY { get => m_ScalarRatioXY; }
        private RectTransform m_rectTransform;
        public RectTransform Rect { get => m_rectTransform; }
        private UnityEngine.Canvas m_Canvas;
        public UnityEngine.Canvas Canv { get => m_Canvas; }

        protected Action m_OnShowed;
        public event Action OnShowed
        {
            add => m_OnShowed += value;
            remove => m_OnShowed -= value;
        }

        protected Action m_OnHidden;
        public event Action OnHidden
        {
            add => m_OnHidden += value;
            remove => m_OnHidden -= value;
        }

        protected Action m_OnStart;
        public event Action OnStart
        {
            add => m_OnStart += value;
            remove => m_OnStart -= value;
        }

        protected virtual void OnEnable() => m_OnShowed?.Invoke();
        protected virtual void OnDisable() => m_OnHidden?.Invoke();
        protected virtual void Start() => m_OnStart?.Invoke();

        protected abstract void OnAwake();
        private void Awake()
        {
            m_CanvasScaler = Extension.FindGameObjectsWithName("Main Canvas")?[0].GetComponent<CanvasScaler>();
            if (m_CanvasScaler == null)
                throw new Exception("The Main Canvas or Canvas Scaler component is undefined.");

            m_Uid = Guid.NewGuid();
            m_ScalarRatio = m_CanvasScaler.GetRation();
            m_ScalarRatioXY = m_CanvasScaler.GetScaleRatio();
            m_rectTransform = GetComponent<RectTransform>();
            m_Canvas = GetComponent<UnityEngine.Canvas>();

            // Add the created instance to the game manager library
            Service<Managers.SceneManager>.Instance.RegisterItem(this.GetType(), this);
            
            OnAwake();
        }

        private void OnDestroy() => Service<Managers.SceneManager>.Instance.UnregisterItem(this.GetType(), this);

        public override bool Equals(object obj)
        {
            ICanvas canvas = obj as ICanvas;
            if (obj != null)
                return m_Uid == canvas.Uid;
            return false;
        }

        public override int GetHashCode() => gameObject.GetHashCode();
    }
}