using UnityEngine;
using Canvas.Core;
namespace Canvas.Infrastructure
{
    public struct IndoorEventArgs : IEventArgs
    {
        public Vector2 Target;
    }
}