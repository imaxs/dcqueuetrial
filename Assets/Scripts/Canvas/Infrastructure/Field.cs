using System;
using Canvas.Core;
using UnityEngine;

namespace Canvas.Infrastructure
{
    public class Field<T> : IField<T>
    {
        private T _value;
        private object _lock = new object();
        public T Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                _OnChanged?.Invoke(value);
            }
        }

        private event Action<T> _OnChanged;
        public event Action<T> OnChanged
        {
            add
            {
                lock (_lock)
                {
                    _OnChanged += value;
                    value?.Invoke(_value);
                }
            }
            remove
            {
                lock (_lock)
                {
                    _OnChanged -= value;
                }
            }
        }

        public Field(T value) => Value = value;
    }
}