using System;
using UnityEngine;
using Canvas.Core;

namespace Canvas.Infrastructure
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    sealed class ServiceFilterSceneAttribute : Attribute
    {
        public string Name;
        public ServiceFilterSceneAttribute(string name) => Name = name;
    }

    public class LocalService<T> : IService<T> where T : class
    {
        private T m_Instance;
        public T Instance { get => m_Instance; }
        private bool m_IsInit;
        public bool IsInit { get => m_Instance != null; }

        public LocalService(T instance) => m_Instance = instance;

        public void Destroy(T instance)
        {
            if (m_Instance == instance) m_Instance = null;
        }
    }

    public static class Service<T> where T : class
    {
        private static LocalService<T> localService;
        public static LocalService<T> GetService()
        {
            if (localService == null) Initialize();
            return localService;
        }

        private static bool m_Dentoyed = false;
        private static object m_Lock = new object();

        public static bool IsInit { get => (localService != null && localService.IsInit); }
        public static T Instance
        {
            get
            {
                Initialize();
                return localService.Instance;
            }
            set
            {
                if (value == null) throw new Exception("Instance value is null");
                if (IsInit) throw new Exception(string.Format("\'{0}\' type already initialized", typeof(T).Name));
                localService = new LocalService<T>(value);
            }
        }

        public static void Destroy(T instance) => localService.Destroy(instance);

        private static void Initialize()
        {
            if (m_Dentoyed)
                throw new Exception(string.Format("[Service] Instance of \'{0}\' already destroyed. ", typeof(T)));

            lock (m_Lock)
            {
                if (!IsInit)
                {
                    var type = typeof(T);

                    // Scriptable objects.
                    if (type.IsSubclassOf(typeof(ScriptableObject)))
                    {
                        var list = Resources.FindObjectsOfTypeAll(type);
                        if (list == null || list.Length == 0)
                            throw new Exception(string.Format("{0}> cannot use an empty type", type.Name));
                        Instance = list[0] as T;
                    }
                    else
                    {
#if UNITY_EDITOR
                        if (type.IsSubclassOf(typeof(Component)) && !type.IsSubclassOf(typeof(MonoService<T>)))
                            throw new Exception(string.Format("\"{0}\" - invalid type. It should be inherited from MonoService", type.Name));
#endif
                        // MonoBehaviour objects.
                        if (type.IsSubclassOf(typeof(MonoService<T>)))
                        {
#if UNITY_EDITOR
                            if (!Application.isPlaying)
                                throw new Exception(string.Format("<{0}> can only be used in Play mode.", type.Name));
                            new GameObject("[Service] " + type.Name).AddComponent(type);
#else
                            new GameObject(type.Name).AddComponent(type);
#endif
                        }
                        else
                        {
                            Instance = Activator.CreateInstance(type) as T;
                        }
                    }
                }
            }
        }
    }
}