using System;
using UnityEngine;
using System.Text.RegularExpressions;

namespace Canvas.Infrastructure
{
    public abstract class MonoService<T> : MonoBehaviour where T : class
    {
        private void Awake()
        {
            if (Service<T>.IsInit)
            {
                DestroyImmediate(this);
                return;
            }

#if UNITY_EDITOR
            var type = GetType();
            var attrs = type.GetCustomAttributes(typeof(ServiceFilterSceneAttribute), true);
            if (attrs.Length > 0)
            {
                var sceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
                var index = attrs.Length - 1;
                bool isMatch = false;
                while (index >= 0)
                    if (Regex.IsMatch(sceneName, ((ServiceFilterSceneAttribute)attrs[index--]).Name))
                    {
                        isMatch = true;
                        break;
                    }

                if (!isMatch)
                    throw new Exception(string.Format("\"{0}\" cannot be used at scene \"{1}\"", type.Name, sceneName));
            }
#endif
            Service<T>.Instance = this as T;
            OnAwake();
        }

        private void OnDestroy()
        {
            if (Service<T>.IsInit)
            {
                OnDestroyed();
                Service<T>.Destroy(this as T);
            }
        }

        protected abstract void OnAwake();
        protected abstract void OnDestroyed();
    }
}