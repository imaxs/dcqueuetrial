using System;
using Canvas.Core;
using System.Collections;
using System.Collections.Generic;

namespace Canvas.Infrastructure
{
    public class Dict : IDict
    {
        private readonly int m_capacity;
        private readonly Dictionary<Type, IList> m_Dictionary;
        public Dict(int capacity = 4)
        {
            m_capacity = capacity;
            m_Dictionary = new Dictionary<Type, IList>(capacity);
        }

        public bool ContainsKey<T>() => m_Dictionary.ContainsKey(typeof(T));

        public void Add<T>(IList items) => m_Dictionary.Add(typeof(T), items);

        public void Add(Type type, object item)
        {
            if (type != item.GetType())
                throw new ArgumentException("The specified type in the parameter doesn't match the type of the object.");

            if (m_Dictionary.ContainsKey(type))
                m_Dictionary[type].Add(item);
            else
                Instantiate(type, item);
        }

        public void Add<T>(T item)
        {
            var type = typeof(T);
            if (m_Dictionary.ContainsKey(type))
                m_Dictionary[type].Add(item);
            else
                m_Dictionary.Add(type, new List<T>(capacity: m_capacity) { item });
        }

        public void Add(object item) => UnityEngine.Debug.Log("add ~ " + item.GetType()); // !!!

        public bool Contains<T>(T item)
        {
            var type = typeof(T);
            if (m_Dictionary.ContainsKey(type))
                foreach (var d in m_Dictionary[type])
                    if (item.Equals(d))
                        return true;
            return false;
        }

        public void Remove<T>(T item) => RemoveItem(typeof(T), item);

        public void Remove(Type type, object item) => RemoveItem(type, item);

        public void Remove(object item) => UnityEngine.Debug.Log("remove ~ " + item.GetType()); // !!!

        public IList TryGet<T>() => m_Dictionary[typeof(T)] ?? null;

        public IList<T> Values<T>(bool isImplementing = false)
        {
            var type = typeof(T);
            IList<T> result = default(IList<T>);

            if (m_Dictionary.ContainsKey(type))
                result = new List<T>(m_Dictionary[type] as IEnumerable<T>);

            if (isImplementing)
                foreach (var items in m_Dictionary)
                    if (items.Key.IsSubclassOf(type) || type.IsAssignableFrom(items.Key))
                        result.AddRange<T>(items.Value as IEnumerable<T>);

            return result;
        }

        public int Length<T>(bool isImplementing = false)
        {
            var type = typeof(T);
            int length = 0;

            if (m_Dictionary.ContainsKey(type))
                length = m_Dictionary[type].Count;

            if (isImplementing)
                foreach (var items in m_Dictionary)
                    if (items.Key.IsSubclassOf(type) || type.IsAssignableFrom(items.Key))
                        length += items.Value.Count;

            return length == 0 ? -1 : length;
        }

        public int Count() => m_Dictionary.Count;

        public IEnumerator<KeyValuePair<Type, IList>> GetEnumerator() => m_Dictionary.GetEnumerator();

        private void RemoveItem(Type type, object item)
        {
            if (m_Dictionary.ContainsKey(type))
            {
                m_Dictionary[type].Remove(item);
                if (m_Dictionary[type].Count == 0)
                    m_Dictionary.Remove(type);
            }
        }

        private void Instantiate(Type type, object item)
        {
            var listType = typeof(List<>);
            var constructedListType = listType.MakeGenericType(type);
            var list = Activator.CreateInstance(constructedListType) as IList;
            if (list != null)
            {
                list.Add(item);
                m_Dictionary.Add(type, list);
            }
            else
                throw new ArgumentNullException("Activator.CreateInstance didn't return an Ilist");
        }
    }
}