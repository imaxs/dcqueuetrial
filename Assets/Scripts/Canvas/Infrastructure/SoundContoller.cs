using System;
using System.Threading.Tasks;
using Canvas.Core;
using UnityEngine;
using Canvas.Managers;
using Canvas.Infrastructure;

namespace Canvas.Infrastructure
{
    public abstract class SoundContoller : ISoundController
    {
        private AudioClip[] m_AudioClips;
        public AudioClip[] AudioClips { get => m_AudioClips; }

        public event Action OnPlay;
        public event Action OnStop;

        private int m_CurrentClipIndex;

        public void Initialize(AudioClip[] audioClips)
        {
            if (audioClips == null || audioClips.Length == 0)
                throw new Exception("AudioClip array should not be empty.");

            m_AudioClips = audioClips;
        }

        public virtual void PlaySound(int clipIndex = 0)
        {
            if (clipIndex >= m_AudioClips.Length)
                throw new Exception("Index was outside the bounds of the AudioClip array.");

            m_CurrentClipIndex = clipIndex;

            var audioClip = m_AudioClips[clipIndex];
            Service<SoundManager>.Instance.PlaySound(audioClip);

            OnPlay?.Invoke();
            if (OnStop != null) InvokeOnStoped(audioClip.length);
        }

        public virtual void StopSound()
        {
            Debug.Log("StopSound Method");
        }

        private async void InvokeOnStoped(float delay)
        {
            await Task.Delay(TimeSpan.FromSeconds(delay));
            OnStop.Invoke();
        }
    }
}