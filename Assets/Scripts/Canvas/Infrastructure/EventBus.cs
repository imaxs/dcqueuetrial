using System;
using Canvas.Core;
using System.Collections.Generic;

namespace Canvas.Infrastructure
{
    public sealed class EventBus : IEventBus
    {
        private readonly IDict m_Mapping;

        public EventBus() => m_Mapping = new Dict();

        public void Subscribe<T>(Action<T> action) where T : IEventArgs => m_Mapping.Add<Action<T>>(action);

        public void Unsubscribe<T>(Action<T> action) where T : IEventArgs => m_Mapping.Remove<Action<T>>(action);

        public void Publish<T>(T args) where T : IEventArgs => Raise<T>(args);

        private void Raise<T>(T args)
        {
            var events = m_Mapping.Values<Action<T>>();
            if (events != null)
                foreach(var e in events)
                    e?.Invoke(args);
        }
    }
}