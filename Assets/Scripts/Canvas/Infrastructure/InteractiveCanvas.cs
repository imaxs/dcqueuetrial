using System;
using Canvas.Core;
using UnityEngine.EventSystems;

namespace Canvas.Infrastructure
{
    public abstract class InteractiveCanvas : BasicCanvas, IInteractiveCanvas, IPointerDownHandler
    {
        protected Action m_OnTapped;
        public event Action OnTapped
        {
            add => m_OnTapped += value;
            remove => m_OnTapped -= value;
        }
        public void OnPointerDown(PointerEventData pointerEventData) => m_OnTapped?.Invoke();
    }
}