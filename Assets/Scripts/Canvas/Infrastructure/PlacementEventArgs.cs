using Canvas.Core;
namespace Canvas.Infrastructure
{
    public struct PlacementEventArgs : IEventArgs
    {
        public int Floor;
        public float X;
        public float Height;
        public float YOffset;
    }
}