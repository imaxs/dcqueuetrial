using System;
using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using Canvas.Core;

namespace Canvas.Contollers
{
    public class SimpleMovementController : IMovementController
    {
        private ICanvas m_View;
        private bool m_isOnStairs;
        private bool m_isReached;
        private IPoints[] m_Points;
        private Transform m_Transform;

        public event Action OnStartMoving;
        public event Action OnMoveCompleted;

        public SimpleMovementController(ICanvas view)
        {
            m_View = view;
            m_Transform = view.Rect.transform;
            m_isOnStairs = false;
            m_isReached = true;
        }

        private Sequence MoveTo(Vector2 target, float speed = 1f)
        {
            float fromX = m_Transform.position.x;
            float duration = Vector2.Distance(m_Transform.position, target) / ((200f + speed) * m_View.ScalarRatio);
            m_View.Rect.localScale = new Vector3(fromX < target.x ? -1 : 1, m_View.Rect.localScale.y, m_View.Rect.localScale.z);
            return DOTween.Sequence().Append(m_View.Rect.DOMove(target, duration));
        }

        private Sequence MoveTo(float x, float speed = 1f)
        {
            float fromX = m_Transform.position.x;
            float duration = Mathf.Abs(x - fromX) / ((200f + speed) * m_View.ScalarRatio);
            m_View.Rect.localScale = new Vector3(fromX < x ? -1 : 1, m_View.Rect.localScale.y, m_View.Rect.localScale.z);
            return DOTween.Sequence().Append(m_View.Rect.DOMoveX(x, duration));
        }

        private Sequence m_Sequence;
        public void MoveToTarget(int floor, float x, float speed = 1f, float height = 0, float yoffset = 0)
        {
            if (!m_isReached || m_isOnStairs) return;

            var currentFloor = (int)Math.Ceiling((m_Transform.position.y - yoffset) / height);

            if (currentFloor == floor)
            {
                m_Sequence = MoveTo(x, speed).OnComplete(()=>{ 
                        OnMoveCompleted?.Invoke();
                        m_isReached = true;
                });
            }
            else
            {
                m_Points = m_Points ?? Extension.Find<IPoints>();
                var points = new List<Vector2[]>();
                foreach (var p in m_Points)
                {
                    float from, to;
                    if (currentFloor < floor)
                    {
                        from = currentFloor * height - height;
                        to = from + height * 2;
                    }
                    else
                    {
                        from = currentFloor * height;
                        to = from - height * 2;
                    }
                    var ps = p.GetPointsBetween(from + yoffset, to + yoffset);
                    if (ps != null) points.Add(ps);
                }
                if (points.Count > 0)
                {
                    var point = GetNearestPoint(points, m_Transform.position.x);
                    m_Sequence = MoveTo(point[0], speed).OnComplete(() =>
                    {
                        m_isOnStairs = true;
                        m_View.Canv.sortingOrder = 2;
                        currentFloor += currentFloor > floor ? -1 : 1;
                        m_Sequence = MoveTo(point[1], speed).OnComplete(() =>
                        {
                            m_isOnStairs = false;
                            m_View.Canv.sortingOrder = 4;
                            m_isReached = true;
                            MoveToTarget(floor, x, speed, height, yoffset);
                        });
                    });
                }else return;
            }
            OnStartMoving?.Invoke();
            m_isReached = false;
        }

        private Vector2[] GetNearestPoint(List<Vector2[]> points, float xpos, int index = 0)
        {
            if (points == null) return null;
            var point = points[0];
            float dist = Mathf.Abs(point[index].x - xpos);
            for (int i = 1; i < points.Count; i++)
            {
                float d = Mathf.Abs(points[i][index].x - xpos);
                if (d < dist)
                {
                    dist = d;
                    point = points[i];
                }
            }
            return point;
        }
    }
}