using System;
using UnityEngine;
using UnityEngine.UI;
using Canvas.Core.Views;
using Canvas.Core.Presenters;
using DG.Tweening;
using Canvas.Core;

namespace Canvas
{
    public class DialogController : IController
    {
        private IDialogView<IDialogPresenter> m_Dialog;
        private Color m_transparent;
        private Sequence m_Sequence;

        public event Action OnShowed;
        public event Action OnHidden;

        public DialogController(IDialogView<IDialogPresenter> dialog)
        {
            m_Dialog = dialog;
            m_transparent = new Color(255, 255, 255, 0);
            m_Dialog.Background.color = m_transparent;
        }

        private void Enable()
        {
            m_Dialog.Rect.anchoredPosition = new Vector2(0, 0);
            switch (m_Dialog.Type)
            {
                case DialogType.Normal:
                    m_Sequence = DOTween.Sequence()
                              .Append(m_Dialog.Rect.DOAnchorPosY(15.0f, 0.85f)).SetLoops(-1, LoopType.Yoyo);
                    break;
                case DialogType.Attention:
                    m_Sequence = DOTween.Sequence()
                              .Append(m_Dialog.Rect.DORotate(new Vector3(0, 0, 2), 0.05f))
                              .Append(m_Dialog.Rect.DORotate(new Vector3(0, 0, -2), 0.10f)).SetLoops(-1, LoopType.Yoyo);
                    break;
            }
        }

        public void Show()
        {
            Enable();
            DOTween.Sequence().Append(m_Dialog.Background.DOFade(1f, 0.5f)).OnComplete(()=>{
                OnShowed?.Invoke();
            });
        }

        public void Hide()
        {
            HideTimer();
            m_Sequence?.Kill();
            DOTween.Sequence().Append(m_Dialog.Background.DOFade(0f, 0.5f)).OnComplete(() =>
            {
                m_Dialog.Background.gameObject.SetActive(false);
                OnHidden?.Invoke();
            });
        }

        public void Success()
        {
            HideTimer();
            if (m_Dialog.Icon != null)
            {
                ShowIcon();
                DOTween.Sequence()
                    .Append(m_Dialog.Icon.rectTransform.DORotate(new Vector3(0, 0, -15), 0.25f))
                    .Append(m_Dialog.Icon.rectTransform.DORotate(new Vector3(0, 0, 30), 0.5f)).SetLoops(2, LoopType.Yoyo).OnComplete(Hide);
            }
        }

        public void Failure()
        {
            HideTimer();
            if (m_Dialog.Icon != null)
            {
                ShowIcon();
                DOTween.Sequence()
                    .Append(m_Dialog.Icon.rectTransform.DORotate(new Vector3(0, 0, -15), 0.05f))
                    .Append(m_Dialog.Icon.rectTransform.DORotate(new Vector3(0, 0, 30), 0.10f)).SetLoops(5, LoopType.Yoyo).OnComplete(Hide);
            }
        }

        private void ShowIcon() => m_Dialog.Icon?.gameObject.SetActive(true);
        private void HideIcon() => m_Dialog.Icon?.gameObject.SetActive(false);

        private void HideTimer()
        {
            if (m_Dialog.Timer != null)
            {
                m_SeqTimer?.Kill();
                m_Dialog.Timer.fillAmount = 0.0f;
            }
        }

        private Sequence m_SeqTimer;
        public void ShowTimer(float duration)
        {
            if (m_Dialog.Timer != null)
            {
                m_Dialog.Timer.fillAmount = 1.0f;
                m_SeqTimer = DOTween.Sequence().Append(m_Dialog.Timer.DOFillAmount(0.0f, duration)).OnComplete(HideTimer);
            }
        }
    }
}