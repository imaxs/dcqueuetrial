using System;
using UnityEngine;
using Canvas.Core;
using DG.Tweening;
using Canvas.Domain;
using Canvas.Infrastructure;
using Canvas.Managers;

namespace Canvas.Contollers
{
    public class SimpleAnimator : IAnimator
    {
        public event Action OnAnimationCompleted;
        private ICanvas m_View;
        private Sequence m_Sequence;

        public SimpleAnimator(ICanvas view)
        {
            m_View = view;
            Idle();
        }

        private bool isKnockAnimPlaying;
        public void Knock()
        {
            if (!isKnockAnimPlaying)
            {
                isKnockAnimPlaying = true;
                m_Sequence = DOTween.Sequence().Append(m_View.Rect.DOScaleY(1.25f, 0.25f)).SetLoops(1, LoopType.Yoyo).OnComplete(() =>
                {
                    OnAnimationCompleted?.Invoke();
                    isKnockAnimPlaying = false;
                });
            }
        }

        private bool isWalkAnimPlaying;
        public void Moving()
        {
            if (!isWalkAnimPlaying)
            {
                m_Sequence.Kill();
                isWalkAnimPlaying = true;
                m_Sequence = DOTween.Sequence().Append(m_View.Rect.DORotate(new Vector3(0, 0, 5), 0.05f))
                                   .Append(m_View.Rect.DORotate(new Vector3(0, 0, -5), 0.10f)).SetLoops(-1, LoopType.Yoyo);
            }
        }

        public void Idle()
        {
            m_Sequence.Kill();
            m_Sequence = DOTween.Sequence().Append(m_View.Rect.DOScaleY(1.0f, 0.3f)).Append(m_View.Rect.DOScaleY(0.95f, 0.3f)).SetLoops(-1);
            isWalkAnimPlaying = false;
        }
    }
}