using UnityEngine;
using Canvas.Core;
using Canvas.Core.Views;
using Canvas.Core.Presenters;
using Canvas.Infrastructure;

namespace Canvas.Domain
{
    public class RoomDoor : InteractiveCanvas
    {
        [SerializeField]
        private GameObject[] m_Dialogs;
        private IDialogView<IDialogPresenter>[] m_DialogViews;

        protected override void OnAwake()
        {
            if (m_Dialogs == null || m_Dialogs.Length == 0)
                Debug.LogWarning("Dialog views are not defined. [" + name + "]");
            else
            {
                m_DialogViews = new DialogView[m_Dialogs.Length];
                for (int i = 0; i < m_Dialogs.Length; i++)
                {
                    m_DialogViews[i] = m_Dialogs[i].GetComponent<IDialogView<IDialogPresenter>>();
                    m_DialogViews[i].OnTapped += () => m_OnTapped?.Invoke();
                }
            }

            //var e = new IndoorEventArgs { Target = scalerRect.TransformPoint(point) };
            m_OnTapped += () => Service<EventBus>.Instance.Publish<IndoorEventArgs>(Event());
        }

        private IndoorEventArgs Event()
        {
            var p = new Vector3[4];
            Rect.GetWorldCorners(p);
            var scalerRect = Scaler.transform.GetComponent<RectTransform>();
            var point = new Vector2((p[2].x + p[1].x) / 2, p[1].y).WorldToLocalPointInRectangle(scalerRect);

            return new IndoorEventArgs { Target = scalerRect.TransformPoint(point) };
        }
    }
}
