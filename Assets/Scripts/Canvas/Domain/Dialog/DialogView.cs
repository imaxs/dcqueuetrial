using System;
using UnityEngine;
using UnityEngine.UI;
using Canvas.Core;
using Canvas.Contollers;
using Canvas.Core.Models;
using Canvas.Core.Views;
using Canvas.Core.Presenters;
using Canvas.Infrastructure;
using Canvas.Domain.Presenters;

namespace Canvas.Domain
{
    [RequireComponent(typeof(Image))]
    public class DialogView : InteractiveCanvas, IDialogView<IDialogPresenter>, IControllable
    {
        private IDialogPresenter m_Presenter;
        public IDialogPresenter Presenter { get => m_Presenter; }

        [SerializeField]
        protected DialogType m_DialogType;
        public DialogType Type { get => m_DialogType; }

        protected Image m_Background;
        public Image Background { get => m_Background; }

        [SerializeField]
        protected Image m_Icon;
        public Image Icon { get => m_Icon; }

        [SerializeField]
        protected Image m_Timer;
        public Image Timer { get => m_Timer; }

        protected IController m_Controller;
        public IController Controller { get => m_Controller; }

        protected override void OnAwake()
        {
            m_Background = GetComponent<Image>();
            if (m_Background == null) 
                Debug.LogWarning("Image component is undefined in [\'" + gameObject.name + "\']");

            m_Controller = new DialogController(this);
            m_Controller.OnShowed += () => m_OnShowed?.Invoke();

            m_Presenter = new DialogPresenter(this);

            gameObject.SetActive(false);
        }

        protected override void OnEnable() => m_Controller.Show();
    }
}