using System;
using System.Threading.Tasks;
using UnityEngine;
using Canvas.Core;
using Canvas.Core.Models;
using Canvas.Infrastructure;

namespace Canvas.Domain.Models
{
    public class DialogModel : IDialogModel
    {
        private readonly IField<int> m_TimerSeconds;
        public IField<int> TimerSeconds { get => m_TimerSeconds; }

        public DialogModel(int initialSeconds)
        {
            m_TimerSeconds = new Field<int>(initialSeconds);
        }

        public async void ActivateTimer()
        {
            int temp = TimerSeconds.Value;
            while(TimerSeconds.Value > 0)
            {
                await Task.Delay(1000);
                TimerSeconds.Value -= 1;
            }
        }
    }
}