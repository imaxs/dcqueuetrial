using System;
using UnityEngine;
using Canvas.Core;
using Canvas.Core.Models;
using Canvas.Core.Views;
using Canvas.Core.Presenters;
using Canvas.Domain.Models;
using Canvas.Infrastructure;

namespace Canvas.Domain.Presenters
{
    public class DialogPresenter : IDialogPresenter
    {
        private readonly IDialogModel m_Model;
        private readonly IDialogView<IDialogPresenter> m_View;

        public event Action OnShowed
        {
            add => m_View.OnShowed += value;
            remove => m_View.OnShowed -= value;
        }
        public event Action OnHidden
        {
            add => m_View.OnHidden += value;
            remove => m_View.OnHidden -= value;
        }
        public event Action OnTapped
        {
            add => m_View.OnTapped += value;
            remove => m_View.OnTapped -= value;
        }

        public DialogPresenter(IDialogView<IDialogPresenter> view)
        {
            m_View = view;
            m_Model = new DialogModel(5);
            OnShowed += () => m_View.Controller.ShowTimer(m_Model.TimerSeconds.Value);
        }

        public void Enable() => m_View.Canv.gameObject.SetActive(true);

        public void Disable() => m_View.Controller.Hide();
    }
}