using Canvas.Core;

namespace Canvas.Domain.Models
{
    public abstract class SettingableModel : ISettingable
    {
        private ISettings m_Dataset;
        public ISettings Dataset { get => m_Dataset; }

        public SettingableModel(ISettings dataset) => m_Dataset = dataset;
    }
}