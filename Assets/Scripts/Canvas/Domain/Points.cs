using System;
using UnityEngine;
using System.Collections.Generic;
using Canvas.Core;
using Canvas.Infrastructure;

namespace Canvas.Domain
{
    public class Points : BasicCanvas, IPoints
    {
        [SerializeField]
        private bool m_TopLeft;
        [SerializeField]
        private bool m_TopRight;
        [SerializeField]
        private bool m_BottomLeft;
        [SerializeField]
        private bool m_BottomRight;

        [SerializeField]
        private Vector2 m_TopLeftOffset;
        [SerializeField]
        private Vector2 m_TopRightOffset;
        [SerializeField]
        private Vector2 m_BottomLeftOffset;
        [SerializeField]
        private Vector2 m_BottomRightOffset;

        private Vector2[] m_Points;
        private RectTransform m_ScalerRect;
        private float m_yOffset;

        public Vector2[] GetPointsBetween(float y1, float y2)
        {
            var from = new Vector2(500, y1).WorldToLocalPointInRectangle(m_ScalerRect);
            var to = new Vector2(500, y2).WorldToLocalPointInRectangle(m_ScalerRect);
            if (y1 < y2)
            {
                var result = new List<Vector2>();
                foreach (var p in m_Points)
                {
                    if (p.y >= from.y && p.y <= to.y)
                        result.Add(m_ScalerRect.TransformPoint(p));
                }

                if (result.Count == 2)
                {
                    var arr = result.ToArray();
                    if (arr[0].y > arr[1].y)
                    {
                        var t = arr[0];
                        arr[0] = arr[1];
                        arr[1] = t;
                    }
                    return arr;
                }
            }
            else
            {
                var result = new List<Vector2>();
                foreach (var p in m_Points)
                {
                    if (p.y <= from.y && p.y >= to.y)
                        result.Add(m_ScalerRect.TransformPoint(p));
                }

                if(result.Count == 2)
                {
                    var arr = result.ToArray();
                    if (arr[0].y < arr[1].y)
                    {
                        var t = arr[0];
                        arr[0] = arr[1];
                        arr[1] = t;
                    }
                    return arr;
                }
            }
            return null;
        }

        protected override void OnAwake()
        {
            int count = 0;
            if (m_TopLeft) count++;
            if (m_TopRight) count++;
            if (m_BottomLeft) count++;
            if (m_BottomRight) count++;

            if (count == 0)
                Debug.LogWarning(String.Format("{0} No points selected.", name));
            else
            {
                m_ScalerRect = Scaler.transform.GetComponent<RectTransform>();

                var p = new Vector3[4];
                Rect.GetWorldCorners(p);

                m_Points = new Vector2[count];
                m_yOffset = (Scaler.referenceResolution.y - Screen.height) * Scaler.scaleFactor;

                if (m_BottomLeft)
                {
                    m_Points[--count] = new Vector2(p[0].x, p[0].y - m_yOffset)  + m_BottomLeftOffset;
                    m_Points[count] = m_Points[count].WorldToLocalPointInRectangle(m_ScalerRect);
                }
                if (m_TopLeft)
                {
                    m_Points[--count] = new Vector2(p[1].x, p[1].y - m_yOffset) + m_TopLeftOffset;
                    m_Points[count] = m_Points[count].WorldToLocalPointInRectangle(m_ScalerRect);
                }
                if (m_TopRight)
                {
                    m_Points[--count] = new Vector2(p[2].x, p[2].y - m_yOffset) + m_TopRightOffset;
                    m_Points[count] = m_Points[count].WorldToLocalPointInRectangle(m_ScalerRect);
                }
                if (m_BottomRight)
                {
                    m_Points[--count] = new Vector2(p[3].x, p[3].y - m_yOffset) + m_BottomRightOffset;
                    m_Points[count] = m_Points[count].WorldToLocalPointInRectangle(m_ScalerRect);
                }
            }
        }
    }
}