using System;
using UnityEngine;
using Canvas.Core;
using Canvas.Core.Models;
using Canvas.Infrastructure;
using Canvas.ScriptableObjects;

namespace Canvas.Domain.Models
{
    public class IndoorModel : SettingableModel, IIndoorModel
    {
        private readonly IField<int> m_StoreysNumber;
        public IField<int> StoreysNumber { get => m_StoreysNumber; }

        private readonly AudioClip[] m_AudioClips;
        public AudioClip[] AudioClips { get => m_AudioClips; }

        public IndoorModel(ISettings settings) : base(settings)
        {
            if (settings.IndoorPrefab == null || settings.IndoorPrefab.Length == 0)
                throw new Exception("IndoorPrefab assets not defined.");

            IndoorPreset preset = settings.IndoorPrefab[0];
            m_StoreysNumber = new Field<int>(preset.StoreysNumber);
            m_AudioClips = preset.AudioClips.Length > 0 ? preset.AudioClips : settings.MusicFiles;
        }
    }
}