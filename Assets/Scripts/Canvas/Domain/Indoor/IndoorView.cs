using Canvas.Core;
using Canvas.Contollers;
using Canvas.Core.Views;
using Canvas.Core.Presenters;
using Canvas.Infrastructure;
using Canvas.Domain.Presenters;

namespace Canvas.Domain
{
    public class IndoorView : InteractiveCanvas, IIndoorView<IIndoorPresenter>
    {
        private IIndoorPresenter m_Presenter;
        public IIndoorPresenter Presenter { get => m_Presenter; }

        private ISoundController m_SoundController;
        public ISoundController SoundController { get => m_SoundController; }

        protected override void OnAwake()
        {
            m_SoundController = new SimpleSoundController();
            m_Presenter = new IndoorPresenter(this);
            m_SoundController.PlaySound();
        }
    }
}