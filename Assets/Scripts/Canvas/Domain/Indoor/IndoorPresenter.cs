using System;
using UnityEngine;
using Canvas.Core;
using Canvas.Core.Models;
using Canvas.Core.Views;
using Canvas.Core.Presenters;
using Canvas.Infrastructure;
using Canvas.ScriptableObjects;
using Canvas.Domain.Models;

namespace Canvas.Domain.Presenters
{
    public class IndoorPresenter : IIndoorPresenter
    {
        private readonly IIndoorModel m_Model;
        private readonly IIndoorView<IIndoorPresenter> m_View;
        private readonly float m_Height, m_MinY;
        private readonly GameObject m_GameObject;
        private readonly IEventBus m_EventBus;

        public IndoorPresenter(IIndoorView<IIndoorPresenter> view)
        {
            m_View = view;
            m_Model = new IndoorModel(Service<PlayerSettings>.Instance);
            m_View.SoundController?.Initialize(m_Model.AudioClips);
            m_GameObject = m_View.Canv.gameObject;

            var v = new Vector3[4];
            m_View.Rect.GetWorldCorners(v);
            m_MinY = v[0].y * m_View.ScalarRatioXY.y;

            for (int i = 0; i < v.Length; i++)
                v[i] = v[i].Scaling(m_View.Scaler);

            m_Height = (v[1].y - v[0].y) / m_Model.StoreysNumber.Value * m_View.ScalarRatioXY.y;

            m_EventBus = Service<EventBus>.Instance;
            m_EventBus.Subscribe<IndoorEventArgs>(OnEvent);

            Enable();
        }

        private void Placement() => Placement(Input.mousePosition);
        private void OnEvent(IndoorEventArgs args) => Placement(args.Target);

        public void Placement(Vector2 target)
        {
            m_EventBus.Publish<PlacementEventArgs>(
                new PlacementEventArgs {
                    Floor = (int)Math.Ceiling((target.y - m_MinY) / m_Height),
                    X = target.x,
                    Height = m_Height,
                    YOffset = m_MinY
                }
            );
        }

        public void Enable()
        {
            if (!m_GameObject.activeSelf) m_GameObject.SetActive(true);
            m_View.OnTapped += Placement;
        }

        public void Disable()
        {
            if (m_GameObject.activeSelf) m_GameObject.SetActive(false);
            m_View.OnTapped -= Placement;
        }
    }
}