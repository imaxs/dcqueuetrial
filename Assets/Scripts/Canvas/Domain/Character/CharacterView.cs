using Canvas.Core;
using Canvas.Core.Views;
using Canvas.Core.Presenters;
using Canvas.Domain.Presenters;
using Canvas.Infrastructure;
using Canvas.Contollers;

namespace Canvas.Domain
{
    public class CharacterView : InteractiveCanvas, ICharacterView<ICharacterPresenter>
    {
        private ICharacterPresenter m_Presenter;
        public ICharacterPresenter Presenter { get => m_Presenter; }

        private IMovementController m_Controller;
        public IMovementController Controller { get => m_Controller; }

        private IAnimator m_Animator;
        public IAnimator Animator { get => m_Animator; }

        private ISoundController m_SoundController;
        public ISoundController SoundController { get => m_SoundController; }

        protected override void OnAwake()
        {
            m_Animator = new SimpleAnimator(this);
            m_Controller = new SimpleMovementController(this);
            m_SoundController = new SimpleSoundController();
            m_Presenter = new CharacterPresenter(this);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            m_Controller.OnStartMoving += () => m_Animator.Moving();
            m_Controller.OnMoveCompleted += () => m_Animator.Idle();
            m_Animator.OnAnimationCompleted += () => m_SoundController.PlaySound();
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            m_Controller.OnStartMoving -= () => m_Animator.Moving();
            m_Controller.OnMoveCompleted -= () => m_Animator.Idle();
            m_Animator.OnAnimationCompleted -= () => m_SoundController.PlaySound();
        }
    }
}