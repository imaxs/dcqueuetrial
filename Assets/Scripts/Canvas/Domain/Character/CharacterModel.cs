using System;
using System.Threading.Tasks;
using UnityEngine;
using Canvas.Core;
using Canvas.Core.Models;
using Canvas.Infrastructure;
using Canvas.ScriptableObjects;

namespace Canvas.Domain.Models
{
    [Serializable]
    public class CharacterModel : SettingableModel, ICharacterModel
    {
        private readonly IField<float> m_SpeedMovement;
        public IField<float> SpeedMovement { get => m_SpeedMovement; }

        private readonly IField<int> m_Level;
        public IField<int> Level { get => m_Level; }

        private readonly AudioClip[] m_AudioClips;
        public AudioClip[] AudioClips { get => m_AudioClips; }

        public CharacterModel(ISettings settings) : base(settings)
        {
            if (settings.CharacterPrefabs == null || settings.CharacterPrefabs.Length == 0)
                throw new Exception("CharacterPrefabs assets not defined.");

            CharacterPreset preset = settings.CharacterPrefabs[0];
            m_SpeedMovement = new Field<float>(preset.SpeedMovement);
            m_Level = new Field<int>(preset.Level);
            m_AudioClips = preset.AudioClips;
        }
    }
}