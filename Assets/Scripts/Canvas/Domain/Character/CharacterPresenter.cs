using System;
using UnityEngine;
using Canvas.Core;
using Canvas.Core.Models;
using Canvas.Core.Views;
using Canvas.Core.Presenters;
using Canvas.Infrastructure;
using Canvas.ScriptableObjects;
using Canvas.Domain.Models;

namespace Canvas.Domain.Presenters
{
    public class CharacterPresenter : ICharacterPresenter
    {
        private readonly ICharacterModel m_Model;
        private readonly ICharacterView<ICharacterPresenter> m_View;
        private readonly GameObject m_GameObject;
        private readonly IEventBus m_EventBus;

        public CharacterPresenter(ICharacterView<ICharacterPresenter> view)
        {
            m_View = view;
            m_Model = new CharacterModel(Service<PlayerSettings>.Instance);
            m_View.SoundController?.Initialize(m_Model.AudioClips);
            m_GameObject = m_View.Canv.gameObject;
            m_EventBus = Service<EventBus>.Instance;
            m_EventBus.Subscribe<PlacementEventArgs>(Move);
            Enable();
        }

        public void Move(PlacementEventArgs args) => 
            m_View.Controller?.MoveToTarget(args.Floor, args.X, m_Model.SpeedMovement.Value, args.Height, args.YOffset);

        public void Tapped()
        {
            m_View.Animator?.Knock();
            m_Model.Level.Value += 1;
        }

        public void Enable()
        {
            if (!m_GameObject.activeSelf) m_GameObject.SetActive(true);
            m_View.OnTapped += Tapped;
            m_Model.Level.OnChanged += (int level) => Debug.Log("Level: " + level);
        }

        public void Disable()
        {
            if (m_GameObject.activeSelf) m_GameObject.SetActive(false);
            m_View.OnTapped -= Tapped;
            m_Model.Level.OnChanged -= (int level) => Debug.Log("Level: " + level);
        }
    }
}