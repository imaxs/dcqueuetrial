using UnityEngine;
namespace Canvas.ScriptableObjects
{
    [CreateAssetMenu(fileName = "CharacterPreset", menuName = "Create/New Character Preset")]
    public class CharacterPreset : ScriptableObject
    {
        [SerializeField]
        private AudioClip[] m_AudioClips;
        public AudioClip[] AudioClips { get => m_AudioClips; }

        [SerializeField]
        [Range(1, 1000)]
        private float m_SpeedMovement;
        public float SpeedMovement { get => m_SpeedMovement; }

        [SerializeField]
        [Range(0, 100)]
        private int m_Level;
        public int Level { get => m_Level; }
    }
}