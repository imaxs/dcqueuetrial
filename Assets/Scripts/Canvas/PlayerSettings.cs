using UnityEngine;
using Canvas.Core;
using Canvas.Core.Presenters;

namespace Canvas.ScriptableObjects
{
    [CreateAssetMenu(fileName = "PlayerSettings", menuName = "Create/New Player Settings")]
    public class PlayerSettings : ScriptableObject, ISettings
    {
        [SerializeField]
        private AudioClip[] m_MusicFiles;
        public AudioClip[] MusicFiles { get => m_MusicFiles; }

        [SerializeField][Range(0, 1)]
        private float m_MusicVolume;
        public float MusicVolume { get => m_MusicVolume; }
        [SerializeField][Range(0, 1)]
        private float m_SoundVolume;
        public float SoundVolume { get => m_SoundVolume; }

        [SerializeField]
        private CharacterPreset[] m_CharacterPrefabs;
        public CharacterPreset[] CharacterPrefabs { get => m_CharacterPrefabs; }

        [SerializeField]
        private IndoorPreset[] m_IndoorPrefab;
        public IndoorPreset[] IndoorPrefab { get => m_IndoorPrefab; }
    }
}