using UnityEngine;

namespace Canvas.ScriptableObjects
{
    [CreateAssetMenu(fileName = "IndoorPreset", menuName = "Create/New Indoor Preset")]
    public class IndoorPreset : ScriptableObject
    {
        [SerializeField]
        private AudioClip[] m_AudioClips;
        public AudioClip[] AudioClips { get => m_AudioClips; }

        [SerializeField]
        [Range(0, 3)]
        private int m_StoreysNumber;
        public int StoreysNumber { get => m_StoreysNumber; }
    }
}