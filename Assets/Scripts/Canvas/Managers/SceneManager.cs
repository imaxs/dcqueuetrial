using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Canvas.Infrastructure;
using Canvas.Core;

namespace Canvas.Managers
{
    public class SceneManager : ISceneManager
    {
        private readonly IDict m_Dictionary;

        public SceneManager() => m_Dictionary = new Dict();

        public void RegisterItem<T>(T item) => m_Dictionary.Add<T>(item);

        public void RegisterItem(Type type, object item) => m_Dictionary.Add(type, item);

        public void UnregisterItem<T>(T item) => m_Dictionary.Remove<T>(item);

        public void UnregisterItem(Type type, object item) => m_Dictionary.Remove(type, item);

        public IList<T> Items<T>(bool isImplementing = false) => m_Dictionary.Values<T>();

        public void PrintDebug()
        {
            Debug.Log("### GameManager ###");
            Debug.Log("Count: " + m_Dictionary.Count());
            Debug.Log("Length (IMovable): " + m_Dictionary.Length<IMovable>(true));
            foreach(var i in m_Dictionary)
                Debug.Log("~ Key: " + i.Key + " Length: " + i.Value.Count);

            IList result = default(IList);
            Debug.Log("IList is null = " + (result == null ? "True" : "False"));
        }
    }
}