using System;
using Canvas.Core;
using UnityEngine;
using Canvas.Infrastructure;

namespace Canvas.Managers
{
    [ServiceFilterSceneAttribute("SampleCanvas")]
    public class SoundManager : MonoService<SoundManager>, ISoundManager
    {
        private bool m_isLastPlayedMusicLooped;
        private AudioSource[] m_AudioSources;

        private float m_SoundVolume;

        protected override void OnAwake()
        {
            DontDestroyOnLoad(gameObject);
            m_AudioSources = new AudioSource[(int)SoundChannel.Max + 1];
            for (var i = 0; i < m_AudioSources.Length; i++)
                m_AudioSources[i] = gameObject.AddComponent<AudioSource>();

            foreach (var audio in m_AudioSources)
            {
                audio.loop = false;
                audio.playOnAwake = false;
            }

            SoundVolume = 1.0f;
        }

        public float SoundVolume
        {
            get
            {
                return m_SoundVolume;
            }
            set
            {
                m_SoundVolume = value;
                foreach (var sound in m_AudioSources)
                    sound.volume = value;
            }
        }

        public AudioSource this[int channel] => m_AudioSources[channel];

        public void PlaySound(string file, bool isLooped = false, SoundChannel channel = SoundChannel.Auto)
        {
            if (string.IsNullOrEmpty(file))
                throw new Exception("The string of sound file name is empty");

            if (channel == SoundChannel.Auto)
                channel = GetFreeSoundChannel();

            if (channel != SoundChannel.None)
                Play(Resources.Load<AudioClip>(file), isLooped, (int)channel);
            else
                Debug.LogWarning("No free channels found for audio playback. [\'" + gameObject.name + "\']");
        }

        public void PlaySound(AudioClip audio, bool isLooped = false, SoundChannel channel = SoundChannel.Auto)
        {
            if (audio == null)
                throw new Exception("The AudioClip file is null");

            if (channel == SoundChannel.Auto)
                channel = GetFreeSoundChannel();

            if (channel != SoundChannel.None)
                Play(audio, isLooped, (int)channel);
            else
                Debug.LogWarning("No free channels found for audio playback. [\'" + gameObject.name + "\']");
        }

        private void Play(AudioClip audio, bool isLooped, int channel)
        {
            if (audio == null)
                throw new Exception("The AudioClip file is null");

            StopSound(channel);

            if (m_SoundVolume > 0f)
            {
                m_AudioSources[channel].clip = audio;
                m_AudioSources[channel].loop = isLooped;
                m_AudioSources[channel].Play();
            }
        }

        public void StopSound(int channel)
        {
            var sound = m_AudioSources[channel];
            if (sound.isPlaying) sound.Stop();
            sound.clip = null;
        }

        protected override void OnDestroyed() { }

        private SoundChannel GetFreeSoundChannel()
        {
            for (int i = 0; i < m_AudioSources.Length; i++)
                if (!m_AudioSources[i].isPlaying) return (SoundChannel)i;
            return SoundChannel.None;
        }
    }
}