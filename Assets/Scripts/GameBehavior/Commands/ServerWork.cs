using GameBehavior.Core;

namespace GameBehavior.Commands
{
    public class ServerWork : ICommand
    {
        private IServerable m_Server;
        public ServerWork(IServerable server) => m_Server = server;
        public virtual void Execute() => m_Server.Serve();
    }
}