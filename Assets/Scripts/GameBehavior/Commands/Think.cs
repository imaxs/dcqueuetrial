using GameBehavior.Core;

namespace GameBehavior.Commands
{
    public class Think : ICommand
    {
        IBrainable m_Brain;
        public Think(IBrainable brain) => m_Brain = brain;
        public virtual void Execute() => m_Brain?.Thinking();
    }
}