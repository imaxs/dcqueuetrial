using GameBehavior.Core;
using GameBehavior.ScriptableObjects;

namespace GameBehavior.Commands
{
    public class Idle<T> : ICommand where T : ICharacteable
    {
        private T m_Character;
        public Idle(T character) => m_Character = character;
        public virtual void Execute() => m_Character.Animator.SetFloat(SceneData.HashSpeed, 0.0f);
    }
}