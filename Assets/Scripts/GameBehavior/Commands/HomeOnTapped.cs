using GameBehavior.Core;
using GameBehavior.Domain;

namespace GameBehavior.Commands
{
    public class HomeOnTapped : ICommand
    {
        private SpawnPoint[] m_Points;
        public HomeOnTapped(SpawnPoint[] points) => m_Points = points;

        public virtual void Execute()
        {
            foreach (var point in m_Points)
                point.LastSpawnedCharacter?.OnTapped();
        }
    }
}