using UnityEngine;
using GameBehavior.Core;
using GameBehavior.Infrastructure;
using GameBehavior.ScriptableObjects;

namespace GameBehavior.Commands
{
    public class SpawnCharacter<T1, T2> : ICommand
        where T1 : IEntity, ISpawnactor<T2>
        where T2 : Character<T2>
    {
        private T1 m_Point;

        public SpawnCharacter(T1 point) => m_Point = point;

        public virtual void Execute()
        {
            var scenedata = SceneData.Instance;
            if (scenedata == null
                || scenedata.CharacterPrefabs.Length == 0
                || scenedata.CharacterPrefabs.Length == 0) return;

            System.Random radnom = new System.Random();
            int randomPrefab = radnom.Next(0, scenedata.CharacterPrefabs.Length);
            int randomCharData = radnom.Next(0, scenedata.CharacterData.Length);
            randomCharData = radnom.Next(0, scenedata.CharacterData.Length);

            var lastCharacter = UnityEngine.GameObject.Instantiate(scenedata.CharacterPrefabs[randomPrefab]).AddComponent<T2>();
            lastCharacter.Init(scenedata.CharacterData[randomCharData], m_Point.Transforms);

            lastCharacter.Transforms.position = m_Point.Transforms.position;
            lastCharacter.Transforms.rotation = m_Point.Transforms.rotation;
            lastCharacter.Object.SetActive(true);

            m_Point.LastSpawnedCharacter = lastCharacter;
        }
    }
}