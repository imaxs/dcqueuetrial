using GameBehavior.Core;
using System.Collections;

namespace GameBehavior.Commands
{
    public class WaitForServed : ICommand
    {
        private ICharacteable m_Character;

        public WaitForServed(ICharacteable character) => m_Character = character;

        public virtual void Execute() => m_Character.Brain?.Thinking();
    }
}