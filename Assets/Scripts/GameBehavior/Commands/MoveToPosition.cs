using UnityEngine;
using System.Collections;
using GameBehavior.Core;
using GameBehavior.ScriptableObjects;

namespace GameBehavior.Commands
{
    public class MoveToPosition<T> : IMovable, ICommand<Vector3, bool> where T : ICharacteable
    {
        private T m_Character;
        public MoveToPosition(T character) => m_Character = character;
        public virtual void Execute(Vector3 target, bool additional = false)
        {
            m_Character.Agent.SetDestination(target);
            m_Character.Animator.SetFloat(SceneData.HashSpeed, m_Character.Agent.speed);
            m_Character.Brain.Thinking(MovingProcess(additional));
        }

        private IEnumerator MovingProcess(bool additional)
        {
            float waitingTime = 1.1f - m_Character.Data.IntelligenceLevel;
            while (m_Character.Agent.pathPending) yield return null;
            while (m_Character.Agent.remainingDistance > m_Character.Agent.stoppingDistance)
                yield return new WaitForSeconds(waitingTime);
            new Commands.Idle<ICharacteable>(m_Character).Execute();
            if (additional) m_Character.Brain.Thinking();
        }
    }
}