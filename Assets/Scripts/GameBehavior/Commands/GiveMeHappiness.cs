using System;
using UnityEngine;
using System.Collections;
using GameBehavior.Core;
using GameBehavior.ScriptableObjects;

namespace GameBehavior.Commands
{
    public class GiveMeHappiness : ICommand
    {
        private ICharacteable m_Character;
        public GiveMeHappiness(ICharacteable characte) => m_Character = characte;
        public virtual void Execute() => m_Character.Animator.SetTrigger(SceneData.HashHappy);
    }
}