using UnityEngine;

namespace GameBehavior.Core
{
    public interface IServerable : IEntity
    {
        Transform Subscribe(ICharacteable client, ICommand request = null);
        void Unsubscribe(ICharacteable client);
        void Serve();
        int QueueSize { get; }
    }
}