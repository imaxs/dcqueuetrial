using UnityEngine;
using System.Collections;

namespace GameBehavior.Core
{
    public interface IBrainstorm
    {
        Coroutine Do(IEnumerator coroutine);
        void Kill(Coroutine coroutine);
    }
}