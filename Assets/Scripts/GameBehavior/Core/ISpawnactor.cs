namespace GameBehavior.Core
{
    public interface ISpawnactor<T>
    {
        T LastSpawnedCharacter { get; set; }
        void Spawn();
    }
}