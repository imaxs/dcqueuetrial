using UnityEngine.EventSystems;

namespace GameBehavior.Core
{
    public interface ITappable : IPointerDownHandler
    {
        void OnTapped();
    }
}