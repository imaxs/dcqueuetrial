namespace GameBehavior.Core
{
    public interface ICommand
    {
        void Execute();
    }

    public interface ICommand<T1>
    {
        void Execute(T1 param);
    }

    public interface ICommand<T1, T2>
    {
        void Execute(T1 param1, T2 param2);
    }

    public interface ICommand<T1, T2, T3>
    {
        void Execute(T1 param1, T2 param2, T3 param3);
    }

    public interface ICommand<T1, T2, T3, T4>
    {
        void Execute(T1 param1, T2 param2, T3 param3, T4 param4);
    }

    public interface ICommand<T1, T2, T3, T4, T5>
    {
        void Execute(T1 param1, T2 param2, T3 param3, T4 param4, T5 param5);
    }

    public interface ICommand<T1, T2, T3, T4, T5, T6>
    {
        void Execute(T1 param1, T2 param2, T3 param3, T4 param4, T5 param5, T6 param6);
    }
}