using UnityEngine;
using System.Collections;

namespace GameBehavior.Core
{
    public interface IBrainable
    {
        void Start(IEnumerator coroutine, ref Coroutine reference);
        void Stop(ref Coroutine coroutine);
        void Thinking(IEnumerator coroutine = null);
        void OnEmotePlayed();
    }
}