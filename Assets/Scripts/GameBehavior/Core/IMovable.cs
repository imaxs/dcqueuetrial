using UnityEngine;

namespace GameBehavior.Core
{
    public interface IMovable
    {
        void Execute(Vector3 target, bool optional = false);
    }
}