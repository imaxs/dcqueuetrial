using UnityEngine;
using UnityEngine.AI;
using GameBehavior.ScriptableObjects;

namespace GameBehavior.Core
{
    public interface ICharacteable : IEntity
    {
        CharacterData Data { get; }
        IMovable Movable { get; }
        Transform StartPoint { get; }
        NavMeshAgent Agent { get; }
        Animator Animator { get; }
        IBrainable Brain { get; }
    }
}