using System;
using UnityEngine;

namespace GameBehavior.Core
{
    public interface IEntity
    {
        GameObject Object { get; }
        Transform Transforms { get; }
        Guid Uid { get; }
    }
}