using UnityEngine;
using System.Collections;
using GameBehavior.Core;
using GameBehavior.Domain;
using GameBehavior.Infrastructure;
using GameBehavior.ScriptableObjects;

namespace GameBehavior.AI
{
    public class PoorBrain : Brain
    {
        protected ICommand m_GiveMeHappiness;
        protected ICharacteable m_Character;
        protected IEntity m_Entity;
        protected IServerable m_Server;

        private SpriteMask m_OrderTimer;

        public PoorBrain(ICharacteable actor = null, IEntity entity = null)
        {
            if (actor != null)
            {
                m_Character = actor;
                m_GiveMeHappiness = new Commands.GiveMeHappiness(m_Character);
            }
            if (entity != null)
            {
                m_Entity = entity;
                m_BrainProcess = m_Entity.Object.AddComponent<Brainstorm>();

                m_OrderTimer = m_Entity.Object.GetComponentInChildren<SpriteMask>();
                m_OrderTimer?.gameObject.SetActive(false);
            }
        }

        Coroutine localCoroutine = null;
        public override void Thinking(IEnumerator coroutine = null)
        {
            Stop(ref localCoroutine);
            if (coroutine == null)
                Thought();
            else
                Start(coroutine, ref localCoroutine);
        }

        private bool m_ReverseMove;
        private bool isAtServer;
        Coroutine timerCoroutine = null;
        private void Thought()
        {
            if (!m_ReverseMove)
            {
                isAtServer = false;
                if (m_Server == null)
                {
                    var servers = SceneData.Instance.Entities<CustomServer>();
                    if (servers != null) m_Server = servers[0] as IServerable;
                }
                if (m_Server != null)
                {
                    var preliminaryPoint = m_Server.Subscribe(m_Character);
                    if (preliminaryPoint == null) return;
                    m_Character.Movable?.Execute(preliminaryPoint.position, true);
                }
                m_ReverseMove = !m_ReverseMove;
            }
            else
            {
                if (!isAtServer && !(m_Character.Agent.remainingDistance > m_Character.Agent.stoppingDistance))
                {
                    isAtServer = true;
                    var actualQueuePoint = m_Server.Subscribe(m_Character, m_GiveMeHappiness);
                    if (actualQueuePoint == null)
                        Thinking();
                    else
                    {
                        m_Character.Movable?.Execute(actualQueuePoint.position);
                        if (m_OrderTimer != null) Start(WaitForServe(m_Character.Data.Patience), ref timerCoroutine);
                    }
                    return;
                }
                Stop(ref timerCoroutine);
                m_OrderTimer?.gameObject.SetActive(false);
                m_Server?.Unsubscribe(m_Character);
                m_Character.Movable?.Execute(m_Character.StartPoint.position);
                m_ReverseMove = !m_ReverseMove;
            }
        }

        private IEnumerator WaitForServe(float patience)
        {
            float time = patience;
            m_OrderTimer.gameObject.SetActive(true);
            while (time > 0)
            {
                time -= Time.deltaTime;
                m_OrderTimer.alphaCutoff = time / patience;
                yield return null;
            }
            m_OrderTimer.gameObject.SetActive(false);
            Thought();
            yield return null;
        }

        public override void OnEmotePlayed()
        {
            if (m_ReverseMove) Thought();
        }
    }
}