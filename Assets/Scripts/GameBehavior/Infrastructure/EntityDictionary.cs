using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameBehavior.Core;

namespace GameBehavior.Infrastructure
{
    public class EntityDictionary
    {
        private readonly Dictionary<Type, IList> m_dictionary = new Dictionary<Type, IList>();

        private void Instantiate(Type type, IEntity value)
        {
            var listType = typeof(List<>);
            var constructedListType = listType.MakeGenericType(type);
            var instance = Activator.CreateInstance(constructedListType) as IList;
            if (instance == null)
                throw new ArgumentNullException("Activator.CreateInstance didn't return an Ilist");
            instance.Add(value);
            m_dictionary.Add(type, instance);
        }

        public void Add<T>(IList value) => m_dictionary.Add(typeof(T), value);
        public void Add(IEntity value) => this.Add(value.GetType(), value);

        public void Add<T>(Type key, List<T> value)
        {
            if (key == null)
                throw new ArgumentNullException("key is null");
            if (value != null && !typeof(List<>).MakeGenericType(key).IsAssignableFrom(value.GetType()))
                throw new ArgumentException(string.Format("doesn't implement List<{0}>", key));
            m_dictionary.Add(key, value);
        }

        public void Add<T>(T value)
        {
            if (m_dictionary.ContainsKey(typeof(T)))
                m_dictionary[typeof(T)].Add(value);
            else
                m_dictionary.Add(typeof(T), new List<T>() { value });
        }

        public void Add(Type type, IEntity value)
        {
            if (m_dictionary.ContainsKey(type))
                m_dictionary[type].Add(value);
            else
                Instantiate(type, value);
        }

        public bool TryGetValue<T>(out IList value)
        {
            foreach (var keyval in m_dictionary)
                if (keyval.Key.IsSubclassOf(typeof(T)))
                {
                    value = keyval.Value;
                    return true;
                }
            value = null;
            return false;
        }

        public IList<T> TryGetValue<T>() where T : class
        {
            IList<T> result = Value<T>();
            foreach (var keyval in m_dictionary)
            {
                if (keyval.Key.IsSubclassOf(typeof(T)))
                {
                    var data = keyval.Value as IList;
                    if (result == null) result = new List<T>(data.Count);
                    foreach (var item in data)
                        result.Add(item as T);
                }
            }
            return result;
        }

        public IList TryGetValue(Type type)
        {
            foreach (var keyval in m_dictionary)
                if (keyval.Key.IsSubclassOf(type))
                    return keyval.Value;
            return null;
        }

        public IList<T> Value<T>()
        {
            if (m_dictionary.ContainsKey(typeof(T)))
                return m_dictionary[typeof(T)] as IList<T>;
            return null;
        }

        public IList Value(Type type)
        {
            if (m_dictionary.ContainsKey(type))
                return m_dictionary[type];
            return null;
        }

        public bool Contains(IEntity value)
        {
            Type type = value.GetType();
            if (!m_dictionary.ContainsKey(type)) return false;
            IList values;
            if (!m_dictionary.TryGetValue(type, out values)) return false;
            foreach (IEntity item in values) if (item.Uid == value.Uid) return true;
            return true;
        }

        public IEnumerator<KeyValuePair<Type, IList>> GetEnumerator()
        {
            return m_dictionary.GetEnumerator();
        }
    }
}