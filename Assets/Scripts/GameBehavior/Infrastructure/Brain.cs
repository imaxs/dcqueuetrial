using UnityEngine;
using System.Collections;
using GameBehavior.Core;

namespace GameBehavior.Infrastructure
{
    public abstract class Brain : IBrainable
    {
        protected IBrainstorm m_BrainProcess;

        protected sealed class Brainstorm : MonoBehaviour, IBrainstorm
        {
            public Coroutine Do(IEnumerator coroutine) => StartCoroutine(coroutine);
            public void Kill(Coroutine coroutine) => StopCoroutine(coroutine);
        }

        public abstract void Thinking(IEnumerator coroutine = null);

        public void Start(IEnumerator coroutine, ref Coroutine reference)
        {
            if (reference == null) reference = m_BrainProcess.Do(coroutine);
        }

        public void Stop(ref Coroutine reference)
        {
            if (reference != null)
                m_BrainProcess.Kill(reference); reference = null;
        }

        public abstract void OnEmotePlayed();
    }
}