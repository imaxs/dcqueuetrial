using UnityEditor;
using UnityEngine.EventSystems;
using GameBehavior.Core;

namespace GameBehavior.Infrastructure
{
    [InitializeOnLoad]
    public abstract class InteractiveEntity<T> : Entity, ITappable where T : IEntity
    {
        protected ICommand m_OnTapped;
        public void OnTapped() => m_OnTapped?.Execute();
        public void OnPointerDown(PointerEventData pointerEventData) => OnTapped();
    }
}