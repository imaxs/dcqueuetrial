using System;
using UnityEngine;
using UnityEngine.AI;
using GameBehavior.Core;
using GameBehavior.ScriptableObjects;

namespace GameBehavior.Infrastructure
{
    public abstract class Character<T> : InteractiveEntity<T>, ICharacteable where T : IEntity
    {
        private CharacterData m_CharacterData;
        public CharacterData Data { get => m_CharacterData; }

        protected IMovable m_Movable;
        public IMovable Movable { get => m_Movable; }

        protected Transform m_StartPoint;
        public Transform StartPoint { get => m_StartPoint; }

        protected NavMeshAgent m_Agent;
        public NavMeshAgent Agent { get => m_Agent; }

        protected Animator m_Animator;
        public Animator Animator { get => m_Animator; }

        protected IBrainable m_Brain;
        public IBrainable Brain { get => m_Brain; }

        protected override void OnAwake()
        {
            base.OnAwake();
            m_Agent = Object.GetComponent<NavMeshAgent>();
            m_Animator = Object.GetComponent<Animator>();
        }

        public void Init(CharacterData data = null, Transform startPosition = null)
        {
            if (data != null)
            {
                m_CharacterData = data;
                m_Agent.speed = m_CharacterData.Speed;
                m_Agent.updateRotation = m_CharacterData.UpdateRotationWhileMoving;
            }
            if (startPosition != null)
            {
                m_StartPoint = startPosition;
            }
        }
    }
}