using System;
using UnityEngine;
using GameBehavior.Core;
using GameBehavior.ScriptableObjects;
using System.Collections.Generic;

namespace GameBehavior.Infrastructure
{
    public abstract class Entity : MonoBehaviour, IEntity
    {
        private Guid m_Uid;
        public Guid Uid { get => m_Uid; }

        private GameObject m_Object;
        public GameObject Object { get => m_Object; }

        private Transform m_Transforms;
        public Transform Transforms { get => m_Transforms; }

        protected void Awake()
        {
            m_Uid = Guid.NewGuid();
            m_Object = transform.gameObject;
            m_Transforms = GetComponent<Transform>();
            SceneData.Instance.AddToLibrary(this);
            OnAwake();
        }

        protected T[] GetComponentsInChildrenWithoutRoot<T>() where T : Component
            => GetComponentsInChildrenWithoutRoot<T>(Object);

        protected T[] GetComponentsInChildrenWithoutRoot<T>(GameObject obj) where T : Component
        {
            var root = obj.GetInstanceID();
            var cmps = obj.GetComponentsInChildren<T>();
            if (cmps == null || cmps.Length == 0 || cmps[0].GetInstanceID() == root) return null;
            T[] result = new T[cmps.Length - 1];
            for (int i = 0; i < result.Length; i++) result[i] = cmps[i + 1];
            return result;
        }

        protected virtual void OnAwake() { }
    }
}