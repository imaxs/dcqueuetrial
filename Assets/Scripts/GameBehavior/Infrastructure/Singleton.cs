using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameBehavior.Infrastructure
{
    public class Singleton<T> : ScriptableObject where T : ScriptableObject
    {
        private static bool m_ShuttingDown = false;
        private static object m_Lock = new object();
        private static T m_Instance;

        public static T Instance
        {
            get
            {
                if (m_ShuttingDown)
                {
                    Debug.LogWarning("[Singleton] Instance '" + typeof(T) + "' already destroyed. Returning null.");
                    return null;
                }

                lock (m_Lock)
                {
                    if (m_Instance == null)
                    {
                        var type = typeof(T);
                        var data = Resources.LoadAll(type.Name, type);
                        if (data != null)
                        {
                            var name = SceneManager.GetActiveScene().name;
                            foreach (var t in data)
                                if (t.name == name && t is T)
                                {
                                    m_Instance = Instantiate<T>(t as T);
                                    break;
                                }
                        }
                    }
                    return m_Instance;
                }
            }
        }

        private void OnDestroy() => m_ShuttingDown = true;
    }
}