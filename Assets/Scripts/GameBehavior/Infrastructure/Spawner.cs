using System;
using UnityEngine;
using GameBehavior.Core;

namespace GameBehavior.Infrastructure
{
    public abstract class Spawner<T1, T2> : Entity, ISpawnactor<T2>
        where T1 : IEntity
        where T2 : Character<T2>
    {
        public T2 LastSpawnedCharacter { get; set; }

        protected ICommand m_Spawn;

        protected override void OnAwake()
        {
            base.OnAwake();
            m_Spawn = new Commands.SpawnCharacter<Spawner<T1, T2>, T2>(this);
        }

        public virtual void Spawn() => m_Spawn.Execute();
    }
}