using System;
using UnityEngine;
using GameBehavior.Core;
using System.Collections.Generic;
using GameBehavior.Infrastructure;

namespace GameBehavior.ScriptableObjects
{
    [CreateAssetMenu(fileName = "SceneData", menuName = "Create/New Scene Data", order = 2)]
    public sealed class SceneData : Singleton<SceneData>
    {
        [SerializeField]
        private GameObject[] m_CharacterPrefabs;
        public GameObject[] CharacterPrefabs { get => m_CharacterPrefabs; }

        [SerializeField]
        private CharacterData[] m_CharacterData;
        public CharacterData[] CharacterData { get => m_CharacterData; }

        private EntityDictionary m_Entities = new EntityDictionary();
        public void AddToLibrary(IEntity entity) => m_Entities.Add(entity);
        public IList<T> Entities<T>() where T : class => m_Entities.TryGetValue<T>();

        [NonSerialized]
        public static int HashSpeed = Animator.StringToHash("Speed");
        [NonSerialized]
        public static int HashHappy = Animator.StringToHash("Happy");
    }
}