using UnityEngine;

namespace GameBehavior.ScriptableObjects
{
    [CreateAssetMenu(fileName = "CharacterData", menuName = "Create/New Character Data", order = 1)]
    public class CharacterData : ScriptableObject
    {
        [SerializeField]
        [Range(1, 60)]
        private float m_Patience;
        [SerializeField]
        [Range(1, 10)]
        private float m_Speed;
        [SerializeField]
        private bool m_UpdateRotationWhileMoving;
        [SerializeField]
        [Range(0, 1)]
        private float m_IntelligenceLevel;

        public virtual float Patience { get => m_Patience; }
        public virtual float Speed { get => m_Speed; }
        public virtual bool UpdateRotationWhileMoving { get => m_UpdateRotationWhileMoving; }
        public virtual float IntelligenceLevel { get => m_IntelligenceLevel; }
    }
}