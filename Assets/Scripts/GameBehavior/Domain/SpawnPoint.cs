using GameBehavior.Infrastructure;

namespace GameBehavior.Domain
{
    public class SpawnPoint : Spawner<SpawnPoint, CustomCharacter>
    {
        protected override void OnAwake()
        {
            base.OnAwake();
            Spawn();
        }
    }
}