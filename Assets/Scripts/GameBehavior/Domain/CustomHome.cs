using GameBehavior.Infrastructure;

namespace GameBehavior.Domain
{
    public class CustomHome : InteractiveEntity<CustomHome>
    {
        private SpawnPoint[] m_spawnPoints;

        protected virtual void Start()
        {
            m_spawnPoints = Object.GetComponentsInChildren<SpawnPoint>();
            m_OnTapped = new Commands.HomeOnTapped(m_spawnPoints);
        }
    }
}