using UnityEngine;
using GameBehavior.Core;
using GameBehavior.Infrastructure;
using GameBehavior.AI;

namespace GameBehavior.Domain
{
    public class CustomCharacter : Character<CustomCharacter>
    {
        protected override void OnAwake()
        {
            base.OnAwake();
            m_Movable = new Commands.MoveToPosition<CustomCharacter>(this);
        }

        protected virtual void Start()
        {
            m_Brain = new PoorBrain(this, this);
            m_OnTapped = new Commands.Think(m_Brain);
        }
    }
}