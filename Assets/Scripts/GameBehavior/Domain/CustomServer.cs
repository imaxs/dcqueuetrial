using UnityEngine;
using GameBehavior.Core;
using System.Collections.Generic;
using GameBehavior.Infrastructure;

namespace GameBehavior.Domain
{
    public class CustomServer : InteractiveEntity<CustomServer>, IServerable
    {
        private int m_QueueSize;
        public int QueueSize { get => m_QueueSize; }

        private Transform[] m_Points;
        protected (ICharacteable, ICommand)?[] m_ClientsQueue;

        public Transform Subscribe(ICharacteable client, ICommand request = null)
        {
            if (request == null || !TryToUpdateCommand(client, request, out int queueItemIndex))
            {
                for (int i = 0; i < m_ClientsQueue.Length; i++)
                {
                    if (!m_ClientsQueue[i].HasValue)
                    {
                        m_QueueSize++;
                        m_ClientsQueue[i] = (client, request);
                        return m_Points[i];
                    }
                }
            }
            else
            {
                if (queueItemIndex > -1)
                {
                    for (int i = 0; i < queueItemIndex; i++)
                    {
                        if (m_ClientsQueue[i].HasValue)
                        {
                            if (m_ClientsQueue[i].Value.Item2 == null)
                            {
                                Swap(i, queueItemIndex);
                                NotifyAll();
                                return m_Points[i];
                            }
                        }
                    }
                    return m_Points[queueItemIndex];
                }
            }
            return null;
        }

        public void Unsubscribe(ICharacteable client)
        {
            for (int i = 0; i < m_ClientsQueue.Length; i++)
                if (m_ClientsQueue[i].HasValue)
                    if (m_ClientsQueue[i].Value.Item1.Uid == client.Uid)
                    {
                        m_QueueSize--;
                        m_ClientsQueue[i] = null;
                        for (int n = i; n < m_ClientsQueue.Length - 1; n++)
                        {
                            int next = n + 1;
                            if (m_ClientsQueue[next].HasValue)
                            {
                                m_ClientsQueue[n] = (m_ClientsQueue[next].Value.Item1, m_ClientsQueue[next].Value.Item2);
                                m_ClientsQueue[next] = null;
                            }
                        }
                        break;
                    }
            NotifyAll();
        }

        public void Serve()
        {
            if (m_ClientsQueue[0].HasValue)
                m_ClientsQueue[0].Value.Item2?.Execute();
        }

        protected override void OnAwake()
        {
            base.OnAwake();
            m_OnTapped = new Commands.ServerWork(this);
            m_Points = GetComponentsInChildrenWithoutRoot<Transform>();
            m_ClientsQueue = new (ICharacteable, ICommand)?[m_Points.Length];
        }

        private bool TryToUpdateCommand(ICharacteable client, ICommand request, out int queueItemIndex)
        {
            for (int i = 0; i < m_ClientsQueue.Length; i++)
                if (m_ClientsQueue[i].HasValue)
                    if (m_ClientsQueue[i].Value.Item1.Uid == client.Uid)
                    {
                        m_ClientsQueue[i] = (client, request);
                        queueItemIndex = i;
                        return true;
                    }
            queueItemIndex = -1;
            return false;
        }

        private void NotifyAll()
        {
            for (int i = 0; i < m_QueueSize; i++)
            {
                if (m_ClientsQueue[i].HasValue)
                    m_ClientsQueue[i].Value.Item1.Movable?.Execute(m_Points[i].position, m_ClientsQueue[i].Value.Item2 == null);
            }
        }

        private void Swap(int forward, int back)
        {
            var temp = (m_ClientsQueue[forward].Value.Item1, m_ClientsQueue[forward].Value.Item2);
            m_ClientsQueue[forward] = (m_ClientsQueue[back].Value.Item1, m_ClientsQueue[back].Value.Item2);
            m_ClientsQueue[back] = temp;
        }
    }
}