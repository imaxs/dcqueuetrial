﻿using UnityEngine;
using GameBehavior.Core;

public class EmoteState : StateMachineBehaviour
{
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //animator.GetComponent<IBrain>().OnEmotePlayed();
       animator.GetComponentInParent<ICharacteable>()?.Brain?.OnEmotePlayed();
    }
}
